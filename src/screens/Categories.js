import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {SIZES, COLORS} from '../constants/theme';

const Categories = () => {
  const category = useSelector(state => state.root.category);
  return (
    <View style={styles.container}>
      {category.map(item => (
        <View style={styles.catCont}>
          <Text style={styles.label}>{item.label}</Text>
        </View>
      ))}
    </View>
  );
};

export default Categories;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.gray,
    paddingHorizontal: SIZES.gutter,
    paddingTop: 20,
  },
  catCont: {
    backgroundColor: COLORS.white,
    paddingVertical: 10,
    paddingHorizontal: SIZES.gutter,
    borderRadius: 9,
    marginBottom: SIZES.gutter - 10,
  },
  label: {
    color: COLORS.black,
    fontWeight: '700',
    fontSize: SIZES.medium,
  },
});
