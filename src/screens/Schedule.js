import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Task from '../components/Task';
import {COLORS, SIZES} from '../constants/theme';
import {useSelector} from 'react-redux';

const Schedule = () => {
  const tasks = useSelector(state => state.root.tasks);
  // console.log(tasks);
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.heading}>Today's Tasks</Text>
      </View>
      {tasks.map(item => (
        <Task
          time="08:00 AM"
          title={item.title}
          category={item.category}
          key={item.title}
        />
      ))}
    </View>
  );
};

export default Schedule;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.gray,
    paddingHorizontal: SIZES.gutter,
  },
  heading: {
    color: COLORS.black,
    fontSize: SIZES.large,
    fontWeight: '700',
    paddingVertical: SIZES.gutter - 10,
  },
});
