import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import * as Animated from 'react-native-animatable';
import {COLORS, SIZES} from '../constants/theme';
import {useDispatch} from 'react-redux';
import {addCategory} from '../redux/slice';

const AddCatModal = () => {
  const [name, setName] = useState('');
  const navigation = useNavigation();
  const dispatch = useDispatch();

  var categories = [];
  const getCategories = async () => {
    // try {
    //   const jsonValue = await AsyncStorage.getItem(CATEGORY_KEY);
    //   return jsonValue != null ? JSON.parse(jsonValue) : null;
    // } catch (e) {
    //   console.log(e);
    // }
  };
  const addCategoryHandler = async name => {
    try {
      dispatch(addCategory({label: name, value: name}));
    } catch (error) {
      console.log(error);
    }
    // try {
    //   // get existing
    //   getCategories().then(res => (categories = res));
    //   // add new category
    //   categories.push({label: name, value: name});
    //   const jsonValue = JSON.stringify(categories);
    //   await AsyncStorage.setItem(CATEGORY_KEY, jsonValue);
    // } catch (error) {
    //   console.log(error);
    // }
    // // console.log();
    // getCategories().then(res => console.log(res));
    setName('');
    navigation.navigate('category');
  };

  return (
    <Animated.View animation="slideInUp" style={styles.container}>
      <Animated.View style={styles.card}>
        <View style={styles.inputCont}>
          <TextInput
            style={styles.input}
            value={name}
            onChangeText={value => setName(value)}
            placeholder="Enter category name"
          />
        </View>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => addCategoryHandler(name)}>
          <Text style={styles.btnTxt}>Add</Text>
        </TouchableOpacity>
      </Animated.View>
    </Animated.View>
  );
};

export default AddCatModal;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.gray,
    paddingHorizontal: SIZES.gutter,
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    backgroundColor: COLORS.white,
    padding: 15,
    borderRadius: 9,
  },
  inputCont: {
    borderColor: COLORS.black,
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10,
  },
  input: {
    color: COLORS.black,
    paddingLeft: 20,
    width: '100%',
  },
  btn: {
    backgroundColor: COLORS.cyan,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 7,
  },
  btnTxt: {
    color: COLORS.black,
    fontSize: SIZES.medium,
    fontWeight: '700',
  },
});
