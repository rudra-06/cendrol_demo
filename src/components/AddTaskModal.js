import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import * as Animated from 'react-native-animatable';
import DropDown from 'react-native-dropdown-picker';
import {COLORS, SIZES} from '../constants/theme';
import {useDispatch, useSelector} from 'react-redux';
import {addTask} from '../redux/slice';

const AddTaskModal = () => {
  const [name, setName] = useState('');
  const [cat, setCat] = useState(null);
  const [open, setOpen] = useState(false);
  const [categories, setCategories] = useState([]);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const category = useSelector(state => state.root.category);

  useEffect(() => {
    setCategories(category);
    // console.log(category);
  }, []);

  const addTaskHandler = async (name, category) => {
    try {
      dispatch(addTask({title: name, category: category}));
    } catch (error) {
      console.log(error);
    }
    setCat(null);
    setName('');
    navigation.navigate('schedule');
  };

  return (
    <Animated.View animation="slideInUp" style={styles.container}>
      <Animated.View style={styles.card}>
        <View style={styles.inputCont}>
          <TextInput
            style={styles.input}
            value={name}
            onChangeText={value => setName(value)}
            placeholder="Enter task name"
          />
        </View>
        <View style={styles.inputCont}>
          <DropDown
            open={open}
            value={cat}
            items={categories}
            setOpen={setOpen}
            setValue={setCat}
            setItems={setCategories}
            placeholder="Select a category"
            dropDownDirection="TOP"
          />
        </View>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => addTaskHandler(name, cat)}>
          <Text style={styles.btnTxt}>Schedule Task</Text>
        </TouchableOpacity>
      </Animated.View>
    </Animated.View>
  );
};

export default AddTaskModal;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.gray,
    paddingHorizontal: SIZES.gutter,
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    backgroundColor: COLORS.white,
    padding: 15,
    borderRadius: 9,
  },
  inputCont: {
    borderColor: COLORS.black,
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10,
  },
  input: {
    color: COLORS.black,
    paddingLeft: 20,
    width: '100%',
  },
  btn: {
    backgroundColor: COLORS.cyan,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 7,
  },
  btnTxt: {
    color: COLORS.black,
    fontSize: SIZES.medium,
    fontWeight: '700',
  },
});
