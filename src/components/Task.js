import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {COLORS, SIZES, STYLES} from '../constants/theme';
import BouncyCheckBox from 'react-native-bouncy-checkbox';

const Task = ({time, title, category}) => {
  const [checked, setChecked] = useState(false);

  return (
    <View style={styles.taskCont}>
      <View style={[STYLES.row]}>
        <View style={styles.timeCont}>
          <Text style={styles.time}>{time}</Text>
        </View>
        <View>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.desc}>{category}</Text>
        </View>
      </View>
      <View>
        <BouncyCheckBox
          size={SIZES.h2}
          fillColor={COLORS.cyan}
          unfillColor={COLORS.white}
          onPress={() => setChecked(!checked)}
        />
      </View>
    </View>
  );
};

export default Task;

const styles = StyleSheet.create({
  taskCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    padding: SIZES.gutter - 10,
    borderRadius: 5,
    marginVertical: SIZES.gutter - 15,
  },
  timeCont: {
    borderColor: COLORS.darkBlue,
    borderWidth: 1,
    borderRadius: 5,
    paddingVertical: SIZES.gutter - 10,
    paddingHorizontal: SIZES.gutter - 15,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  time: {
    color: COLORS.darkBlue,
    fontWeight: '800',
    fontSize: SIZES.tiny,
  },
  title: {
    color: COLORS.black,
    fontSize: SIZES.body,
    fontWeight: '800',
    letterSpacing: 1,
  },
  desc: {
    color: COLORS.pink,
    fontSize: SIZES.small,
    fontWeight: '600',
    letterSpacing: 0.6,
  },
});
