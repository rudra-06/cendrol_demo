export const COLORS = {
  black: '#303030',
  gray: '#e8e8e8',
  cyan: '#31e1f5',
  pink: '#f551f5',
  red: '#f03232',
  white: '#ffffff',
  darkBlue: '#00078c',
};

export const SIZES = {
  body: 20,
  gutter: 20,
  medium: 17,
  small: 14,
  tiny: 12,
  large: 22,
  h2: 24,
  h1: 26,
};

export const STYLES = {
  row: {
    flexDirection: 'row',
  },
};
