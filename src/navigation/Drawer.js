import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import ScheduleScreen from '../screens/Schedule';
import CategoryScreen from '../screens/Categories';
import AddTaskModal from '../components/AddTaskModal';
import AddCatModal from '../components/AddCatModal';

const Drawer = () => {
  const navigation = useNavigation();
  const Drawer = createDrawerNavigator();

  const HeaderRightSchedule = () => (
    <TouchableOpacity
      style={{marginRight: 15}}
      onPress={() => navigation.navigate('addTask')}>
      <Icon name="plus-box-outline" size={24} color="#000000" />
    </TouchableOpacity>
  );

  const HeaderRightCategory = () => (
    <TouchableOpacity
      style={{marginRight: 15}}
      onPress={() => navigation.navigate('addCat')}>
      <Icon name="plus-box-outline" size={24} color="#000000" />
    </TouchableOpacity>
  );

  return (
    <Drawer.Navigator backBehavior="history">
      <Drawer.Screen
        name="schedule"
        component={ScheduleScreen}
        options={{
          headerTitle: 'Schedule',
          headerTitleAlign: 'center',
          headerRight: () => <HeaderRightSchedule />,
          drawerType: 'slide',
        }}
      />
      <Drawer.Screen
        name="category"
        component={CategoryScreen}
        options={{
          headerTitle: 'Categories',
          headerTitleAlign: 'center',
          headerRight: () => <HeaderRightCategory />,
          drawerType: 'slide',
        }}
      />
      <Drawer.Screen
        name="addTask"
        component={AddTaskModal}
        options={{
          drawerItemStyle: {height: 0},
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="addCat"
        component={AddCatModal}
        options={{
          drawerItemStyle: {height: 0},
          headerShown: false,
        }}
      />
    </Drawer.Navigator>
  );
};

export default Drawer;
