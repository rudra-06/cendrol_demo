import {createSlice} from '@reduxjs/toolkit';

export const rootSlice = createSlice({
  name: 'root',
  initialState: {
    tasks: [],
    category: [],
  },
  reducers: {
    addTask: (state, action) => {
      state.tasks.push(action.payload);
    },
    addCategory: (state, action) => {
      state.category.push(action.payload);
    },
  },
});

export const {addCategory, addTask} = rootSlice.actions;
export default rootSlice.reducer;
