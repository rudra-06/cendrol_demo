import * as ActionType from './actionTypes';

const initialState = {
  tasks: [],
  category: [],
};

export const TodoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.TASK_ADDED:
      return {
        ...state,
        tasks: [...state.tasks, action.payload],
      };
    case ActionType.CATEGORY_ADDED:
      return {
        ...state,
        category: [...state.category, action.payload],
      };
    default:
      return state;
  }
};
