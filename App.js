import React from 'react';
import 'react-native-gesture-handler';
import {View, Text} from 'react-native';
import {store} from './src/redux/store';
import {Provider} from 'react-redux';
import Root from './src/navigation/Root';

const App = () => {
  return (
    <Provider store={store}>
      <Root />
    </Provider>
  );
};

export default App;
